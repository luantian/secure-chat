Emil Landron
Jose Ventura
Mauricio Suarez 

- This project is a secure chat application that uses the python framework Twisted to connect the server and the client using SSL. http://twistedmatrix.com/trac/

- The repo contains a total of 4 files, a server, a client, server private key, and server certificate. To start running the secure chat, one must start the server first (it is recommend to run it with a sudo command) then run the client. 

- The server will start listening with SSL using the key and certificated describe above in order to secure and certify the connection with the client. A step by step give on how the key and self signed certificate were obtain is presented here. http://www.akadia.com/services/ssh_test_certificate.html

- The client uses the wx framework for the GUI so it must be installed first. A GUI was require because if the client is run from the terminal it wont update itself when the server sends a command, because it will be stock waiting for user input. http://www.wxpython.org